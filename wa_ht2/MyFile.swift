//
//  MyFile.swift
//  wa_ht2
//
//  Created by Oleg Brichak on 12/12/17.
//  Copyright © 2017 Oleg Brichak. All rights reserved.
//

import UIKit

class MyFile: NSObject {

    static func whatDay(_ value: Int){
        var day = value
        
        if(day>7){
            day = day%7
        }
        
        if(day==1){
            print("Понедельник")
        }
        if(day==2){
            print("Вторник")
        }
        if(day==3){
            print("Среда")
        }
        if(day==4){
            print("Четверг")
        }
        if(day==5){
            print("Пятница")
        }
        if(day==6){
            print("Субботу")
        }
        if(day==7){
            print("Воскресение")
        }
        
    }
    
    static func helloWorld() {
        print("Привет")
    }
    
    static func getSum (value1: Int, value2: Int){
        print("Сумма чисел \(value1) и \(value2) равна \(value1+value2)")
    }
    
    static func makeSomeMagic (_ value: Int) -> Int {
        var result = 0
        result = (value*30)+5
        return result
    }

    
}

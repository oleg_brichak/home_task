//
//  ViewController.swift
//  wa_ht2
//
//  Created by Oleg Brichak on 12/12/17.
//  Copyright © 2017 Oleg Brichak. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
// Домашка №2.
        findMaxOfTwo(10, 24)
        task_1_1(value: 4)
        printNumbers(value: 4)
        getNumberOfDeviders(value: 28)
        isPerfectNumber(496)
        print("Задача 2-1. Вложив 24$ на 191 год под 6%, вы получите \(String(format: "%.2f",calculatePercents(value: 24, percent: 0.06, duration: 2017-1826 , summarize: false)))")
        howMuchINeedToSurvive(income: 1000, outcome: 1200, time: 10)
        howLongICanSurvive(savings: 5000, income: 1000, outcome: 1200, percent: 0.03)
        reverseValue(value: 873)
        
        
// работа в классе. Лекция 4
//        MyFile.helloWorld()
//        MyFile.whatDay(9)
//        MyFile.getSum(value1: 5, value2: 10)
//
//        var value:Int
//        value = 10
//        print("число \(value), результат = \(MyFile.makeSomeMagic(value))")
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func task_1_1 (value: Int) {
        elevateNumber(value: value, exponent: 2)
        elevateNumber(value: value, exponent: 3)
    }
    
    func howMuchINeedToSurvive(income: Int, outcome: Int, time: Int) {
        
        var totalOutcome = calculatePercents(value: outcome, percent: 0.03, duration: time, summarize: true)
        print("Общие расходы за \(time) месяцев - \(String(format: "%.2f",totalOutcome)) грн.")

        totalOutcome = totalOutcome - Double(income*time)
        print("Вам необходимо \(String(format: "%.2f",totalOutcome)) грн., чтобы выжить")
        print()
    }
    
    func howLongICanSurvive(savings : Int, income: Int, outcome: Int, percent :Double){
        var time = 0
        var rest = Double(savings)
        var monthlyOutcome = Double(outcome)
        
        while rest>=0 {
            rest = rest + Double(income) - monthlyOutcome
            monthlyOutcome = monthlyOutcome + (monthlyOutcome * percent)
            time += 1
        }
        
        print("Задача 2-3. Имея \(savings) грн. сбережений, и получая \(income) степендии, Вы можете прожить \(time) месяцев")
    }
    
    func calculatePercents (value: Int, percent: Double, duration: Int, summarize: Bool) ->Double
    {
        var result: Double
        var sum = 0.0
        
        result = Double(value)
        
        for _ in 0..<duration {
            result = result + (result * percent)
            sum += result
//            print(result)
        }
        
        if(summarize) {
            return sum
        } else {
            return result
        }
    }
    
    func reverseValue(value: Int)
    {
        var result:Int
        
        result = (value%10)*100
                 + (numberOfDevides(value: value, devlider: 10)%10)*10
                 + numberOfDevides(value: value, devlider: 100)
        
        print("Задача 2-4. Развернуть число \(value). Результат - \(result)")
        print()
    }
    
    func numberOfDevides (value: Int, devlider: Int) -> Int {
        var result: Int
        
        result = value%devlider
        result = (value-result)/devlider
        
        return result
    }

    func findMaxOfTwo (_ value1 :Int, _ value2 : Int){
        var result = value2
        
        if(value1>value2){
            result = value1
        }
        
        print("Задание 1-0. Из чисел \(value1) и \(value2), большее - \(result)")
        print()
    }
    
    func elevateNumber (value :Int, exponent :Int){
        var result = value
        
        for _ in 0..<exponent-1 {
            result = result * value
        }
        
        print("Задание 1-1. Возвести число \(value) в степень \(exponent) = \(result)")
        print()
    }
    
    func printNumbers (value :Int) {
        print("Задача 1-2. Вывести на экран все числа до заданного и в обратном порядке до 0")
        var reverse = value
        
        for i in 0..<value+1 {
            print("\(i) \(reverse)")
            reverse -= 1
        }
        
        print()
    }
    
    func getNumberOfDeviders (value :Int) {
        var result = 0
        print("Задача 1-3. Количество делителея для числа \(value)")
        print("Список делителей :")
        
        for i in 1..<value+1 {
            if(value%i==0) {
                print("\(i)")
                result += 1
            }
        }
        
        print("Общее кол-во делителей - \(result)")
        print()
    }
    
    func isPerfectNumber (_ value :Int) {
        print ("Задача 1-4. Проверить, является ли число \(value) совершенным.")
        var sum = 0
        
        for i in 1..<value {
            if(value%i==0) {
               sum = sum + i
            }
        }
        
        if (sum == value) {
            print("Результат: число \(value) является совершенным")
        }else {
            print("Результат: число \(value) НЕ является совершенным")
        }
        print()
    }
   
}

